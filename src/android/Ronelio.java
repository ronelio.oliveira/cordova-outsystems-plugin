package com.ronelio;

import android.content.Context;
import android.content.SharedPreferences;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;

/**
* This class echoes a string called from JavaScript.
*/
public class Ronelio extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        switch (action) {
            case "storeData":
                String key = args.getString(0);
                String value = args.getString(1);
                this.storeData(key, value);
                callbackContext.success();
                return true;
            case "readData":
                key = args.getString(0);
                String result = this.readData(key);
                callbackContext.success(result);
                return true;
            case "destroyData":
                key = args.getString(0);
                this.destroyData(key);
                callbackContext.success();
                return true;
        }
        return false;
    }

    private void storeData(String key, String value) {
        SharedPreferences sharedPref = this.getSharedPreferences();
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.apply();
    }

    private String readData(String key) {
        SharedPreferences sharedPref = this.getSharedPreferences();
        return sharedPref.getString(key, "");
    }

    private void destroyData(String key) {
        SharedPreferences sharedPref = this.getSharedPreferences();
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove(key);
        editor.apply();
    }

    private SharedPreferences getSharedPreferences() {
        Context context = this.cordova.getActivity().getApplicationContext();
        return context.getSharedPreferences("applicationPreferences", Context.MODE_PRIVATE);
    }
}