
    var cordova = require('cordova');
 
    function Ronelio() {
        this.pluginName = "Ronelio";
    }
 
    Ronelio.prototype.storeData = function (key, value) {
       cordova.exec(function (winParam) {
             console.log(winParam)
          },
          function (error) {
             alert(error)
          },
          this.pluginName,
          "storeData",
          [key, value]);
    }

    Ronelio.prototype.readData = function(key, callback){
        cordova.exec(
            function(result){
                callback(result);
            },
            function(error){
                alert(error);
            },
            this.pluginName,
            "readData",
            [key]
        );
    }

    Ronelio.prototype.destroyData = function(key, callback){
        cordova.exec(
            function(result){
                callback(result);
            },
            function(error){
                alert(error);
            },
            this.pluginName,
            "destroyData",
            [key]
        );
    }

    module.exports = new Ronelio();